import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Place } from 'src/app/places/places.model';
import { ModalController } from '@ionic/angular';
import { FormGroup, NgForm } from '@angular/forms';

@Component({
  selector: 'app-creat-booking',
  templateUrl: './creat-booking.component.html',
  styleUrls: ['./creat-booking.component.scss'],
})
export class CreatBookingComponent implements OnInit {

  @Input() selectedPlace: Place ;
  @Input() theMode: string ;
  show: boolean ;
  @ViewChild('f') form : NgForm ;
  constructor(private modalCtrl: ModalController) { }
  ngOnInit() {
    if( this.theMode === 'selected' ) {
      this.show = true ;
    }else {
      this.show = false ;
    }
  }


  onSubmit() {
    const myf = this.form.form.value ;
    this.modalCtrl.dismiss({
      placeid : this.selectedPlace.id ,
      placetitile: this.selectedPlace.title,
      img : this.selectedPlace.img,
      guest: myf.gnumber ,
      firstname: myf.fname,
      lastname: myf.lname,
      from: myf.from,
      to: myf.to
    } , 'confirme');

  }

  dismiss() {
    this.modalCtrl.dismiss({message: 'nothhing'} , 'dissmised');
  }

}

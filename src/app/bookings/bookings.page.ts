import { Component, OnInit, OnDestroy } from '@angular/core';
import { BookingService } from './booking.service';
import { Booking } from './booking.model';
import { Subscription } from 'rxjs';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.page.html',
  styleUrls: ['./bookings.page.scss'],
})
export class BookingsPage implements OnInit , OnDestroy {

  constructor(
    private bookinService: BookingService ,
    private alertCtrl: AlertController
    ) { }
  Bookings: Booking[];
  mySub: Subscription ;
  ngOnInit() {
    this.mySub =this.bookinService.Bookings.subscribe( bookings => {
      this.Bookings = bookings ;
    }) ;
  }

  DeleteBooking(slide , id: string) {
    this.alertCtrl.create(
     {
        header: 'Cancel the Booking' ,
        message: 'Are You Sure you Want to Cancel !',
        buttons: [
          {
            text: 'Cancel',
            handler: () => {
              slide.close() ;
            }
          },
          {
            text: 'Delete',
            handler: () => {
              slide.close() ;
              this.bookinService.CancelBooking(id).subscribe() ;
            }
          }
        ]
      }
    ).then(alert => {
      alert.present() ;
    })
  }
  ngOnDestroy() {
    this.mySub.unsubscribe() ;
  }

}

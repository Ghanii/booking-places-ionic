export class Booking {
    constructor(
        public id: string ,
        public placeId: string ,
        public userId: string ,
        public placeTitle: string ,
        public img: string ,
        public guestNumber: number ,
        public firstName: string,
        public lastName: string,
        public From: Date,
        public To: Date
    ) {}
}
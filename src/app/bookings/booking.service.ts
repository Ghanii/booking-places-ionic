import { Injectable } from '@angular/core';
import { Booking } from './booking.model';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import {take , tap} from 'rxjs/operators' ;

@Injectable({providedIn: 'root'})
export class BookingService {
    private _bookings = new BehaviorSubject<Booking []>([]) ;

    get Bookings() {
        return this._bookings.asObservable() ;
    }
    constructor( private authS: AuthService ) {}

    addBookin(
        placeId: string,
        placetitle: string,
        img: string,
        guestNumber: number ,
        firstname: string,
        lastname: string,
        from: Date,
        to: Date
    )
    {
        const newBooking = new Booking(
            Math.random().toString() ,
            placeId,
            this.authS.getUserId(),
            placetitle,
            img,
            guestNumber,
            firstname,
            lastname,
            from,
            to
        ) ;
        this.Bookings.pipe(take(1)).subscribe( bookings => {
            this._bookings.next(bookings.concat(newBooking)) ;
        } ) ;

    }


    CancelBooking(id: string) {
       return this.Bookings.pipe(take(1) , tap(data => {
           const dt = data.filter(bks => {return bks.id !== id});
           console.log(dt) ;
           console.log('this is the id :' , id) ;
            this._bookings.next(dt) ;
        })) ;
    }
}
import { Injectable } from '@angular/core';
import { Place } from './places.model';
import { AuthService } from '../auth/auth.service';
import { BehaviorSubject, concat } from 'rxjs';
import {take , map} from 'rxjs/operators' ;

@Injectable({
  providedIn: 'root'
})
export class PlacesService {
  private _Places =  new BehaviorSubject<Place[]>(
    [
      new Place(
        'p1' ,
        'paris',
        'LAMOUR TOUJOURS',
        '../../assets/images/paris.jpg',
        199.22,
        new Date('2020-05-12'),
        new Date('2022-07-11') ,
        'abc'
      ),
      new Place(
        'p2' ,
        'new York',
        'MANHATTAN',
        '../../assets/images/manhattan.jpg',
        199.99,
        new Date('2020-05-12'),
        new Date('2022-07-11') ,
        'xyz'
      ),
      new Place(
        'p3' ,
        'maroc',
        'tangier beautiful',
        '../../assets/images/tangier.jpg',
        100.55,
        new Date('2020-05-12'),
        new Date('2022-07-11') ,
        'xyz'
      ),
    ]
  )  ;

  get Places() {
    return this._Places.asObservable();
  }

  getplaceById(id: string) {
    return this.Places.pipe(take(1) , map(places => {
      return places.find(place => {
        return place.id === id ;
      })
    })) ;
  }

  addPlace(title: string , description: string , price: number , dateFrom: Date , dateTo: Date) {
     const newPlace = new Place(
      Math.random.toString(),
      title ,
      description ,
      '../../assets/images/tangier.jpg' ,
      price ,
      dateFrom ,
      dateTo ,
      this.authS.getUserId()
      ) ;
      this.Places.pipe(take(1)).subscribe( place => {
        console.log(place) ;
       this._Places.next(place.concat(newPlace)) ;
      } )
  }

  UpdatePlace(id: string , title: string , description: string) {
    this.Places.pipe(take(1) , map(places => {
      const index = places.findIndex( plc => { return plc.id === id }) ;
      const oldplaces = places ;
      const updatedplcaes = oldplaces ;
      const oldindex = oldplaces[index] ;
      updatedplcaes[index] = new Place(
        oldindex.id ,
        title ,
        description,
        oldindex.img ,
        oldindex.cost,
        oldindex.dateFrom,
        oldindex.dateTo,
        oldindex.userId
        ) ;
      return updatedplcaes ;
    })).subscribe(newPlaces => {
      this._Places.next(newPlaces) ;
    })
  }


  constructor(private authS: AuthService) { }
}

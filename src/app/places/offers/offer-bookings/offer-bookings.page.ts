import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-offer-bookings',
  templateUrl: './offer-bookings.page.html',
  styleUrls: ['./offer-bookings.page.scss'],
})
export class OfferBookingsPage implements OnInit {

  constructor(private activeRout: ActivatedRoute , private route: Router) { }
  id: string ;
  ngOnInit() {
    this.activeRout.paramMap.subscribe(param => {
      this.id = param.get('placeId') ;
    });
  }


}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { PlacesService } from '../places.service';
import { Router } from '@angular/router';
import { Place } from '../places.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.page.html',
  styleUrls: ['./offers.page.scss'],
})
export class OffersPage implements OnInit , OnDestroy {

  constructor(private placeService: PlacesService , private router: Router) { }

  Places: Place[];
  mySub: Subscription
  ngOnInit() {
    this.mySub = this.placeService.Places.subscribe(places => {
    this.Places = places ;
   })
  }
  onSlide(slide , id: string) {
    slide.close();
    this.router.navigate(['/','places','tabs','offers','edit',id]);
  }

  ngOnDestroy() {
    if( this.mySub ) {
      this.mySub.unsubscribe() ;
    }
  }

}

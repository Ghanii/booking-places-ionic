import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PlacesService } from '../../places.service';
import { Place } from '../../places.model';
import { Subscription } from 'rxjs';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-edit-offer',
  templateUrl: './edit-offer.page.html',
  styleUrls: ['./edit-offer.page.scss'],
})
export class EditOfferPage implements OnInit , OnDestroy {

  constructor(
    private activeRoute: ActivatedRoute ,
     private placeService: PlacesService,
     private router: Router ,
     private loadinCtrl : LoadingController
     ) { }
  id: string ;
  form: FormGroup;
  place: Place ;
  mySub: Subscription ;
  ngOnInit() {
    this.activeRoute.paramMap.subscribe(param => {
      this.id = param.get('placeId') ;

     this.mySub = this.placeService.getplaceById(this.id).subscribe( place => {
        this.place = place ;
      });

      this.form = new FormGroup({
        title: new FormControl( this.place.title, {
          updateOn: 'blur',
          validators: [Validators.required]
        }) ,
        description : new FormControl(this.place.description, {
          updateOn: 'blur' ,
          validators: [Validators.required , Validators.maxLength(180)]
        })
      });
    })
  }

  onUpdate() {
    const f = this.form.value ;
    this.placeService.UpdatePlace(this.id , f.title , f.description) ;
    this.showSpiner() ;
    setTimeout( ()=> {
      this.loadinCtrl.dismiss() ;
      this.router.navigate(['/' ,'places' , 'tabs' ,'offers']);
    } , 1500 );
  }

  ngOnDestroy() {
    if(this.mySub) {
      this.mySub.unsubscribe() ;
    }
  }

  private showSpiner() {
    this.loadinCtrl.create( {message : 'updating ...'} )
    .then(spiner => {
      spiner.present() ;
    })
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { Place } from '../../places.model';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.scss'],
})
export class OfferComponent implements OnInit {

  constructor() { }
  @Input() place : Place ;

  ngOnInit() {}
}

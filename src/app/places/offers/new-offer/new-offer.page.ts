import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PlacesService } from '../../places.service';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-new-offer',
  templateUrl: './new-offer.page.html',
  styleUrls: ['./new-offer.page.scss'],
})
export class NewOfferPage implements OnInit {
  constructor(
    private placeS: PlacesService ,
     private router: Router,
     private loadCtrl: LoadingController
     ) { }
  form: FormGroup ;
  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl('parsi' , {
        updateOn: 'blur',
        validators: [Validators.required]
      }) ,
      description: new FormControl('ghani' , {
        updateOn: 'blur' ,
        validators: [Validators.required ,Validators.maxLength(120)]
      }),
      price: new FormControl('12.99' , {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      dateFrom: new FormControl('null' , {
        updateOn:'blur',
        validators: [Validators.required]
      }),
      dateTo : new FormControl(null , {
        updateOn: 'blur',
        validators: [Validators.required]
      })

    }) ;
  }
  CreateOffer() {
    console.log(this.form.value.dateTo);
    const f = this.form.value ;
    this.placeS.addPlace( f.title , f.description ,f.price ,f.dateFrom , f.dateTo) ;
    this.loadSpin() ;
    setTimeout( () => {
      this.loadCtrl.dismiss() ;
      this.router.navigate(['/' , 'places' , 'tabs','offers']);
    } , 1500 );
  }

  private loadSpin() {
    this.loadCtrl.create( { keyboardClose:true , message:'adding offer ...'  })
    .then(load => {
      load.present() ;
    })
  }

}

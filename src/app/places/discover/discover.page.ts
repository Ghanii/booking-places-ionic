import { Component, OnInit, OnDestroy } from '@angular/core';
import { PlacesService } from '../places.service';
import { Place } from '../places.model';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.page.html',
  styleUrls: ['./discover.page.scss'],
})
export class DiscoverPage implements OnInit , OnDestroy {

  constructor(private places: PlacesService , private authS: AuthService) { }
  loadedPlaces: Place[] ;
  mySub: Subscription ;
  AllPlaces: Place[] ;
  ngOnInit() {
   // this.loadedPlaces = this.places.Places ;
    this.mySub =  this.places.Places.subscribe( places => {
      this.AllPlaces = places ;
      this.loadedPlaces = this.AllPlaces ;
    });
  }
  onFilter(event) {
    console.log(event.detail) ;
    if(event.detail.value === 'FILTRED') {
      this.loadedPlaces = this.AllPlaces.filter(plc => {
         return plc.userId ===  this.authS.getUserId() ;
        })
    }
    else {
      this.loadedPlaces = this.AllPlaces ;
    }
  }

  ngOnDestroy() {
    if(this.mySub) {
      this.mySub.unsubscribe() ;
    }
  }
}

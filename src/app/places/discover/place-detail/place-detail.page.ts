import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Place } from '../../places.model';
import { PlacesService } from '../../places.service';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { CreatBookingComponent } from 'src/app/bookings/creat-booking/creat-booking.component';
import { Subscription } from 'rxjs';
import { BookingService } from 'src/app/bookings/booking.service';

@Component({
  selector: 'app-place-detail',
  templateUrl: './place-detail.page.html',
  styleUrls: ['./place-detail.page.scss'],
})
export class PlaceDetailPage implements OnInit , OnDestroy {

  constructor(
    private activeRoute: ActivatedRoute ,
    private placeSerivce: PlacesService,
    private modalCtrl: ModalController ,
    private actionSheet: ActionSheetController,
    private bookingS: BookingService
     ) { }
  Place: Place ;
  mySub: Subscription ;
  ngOnInit() {
    this.activeRoute.paramMap.subscribe(param => {
     this.mySub = this.placeSerivce.getplaceById(param.get('placeId')).subscribe( place => {
        this.Place = place ;
      });
    })
  }

  onBook() {
    this.actionSheet.create({
      header: 'Chose an Action' ,
      buttons : [
        {
          text: 'random',
          handler : () => {
            this.openModel('random') ;
          }
        },
        {
          text: 'selected',
          handler : () => {
            this.openModel('selected') ;
          }
        }, {
          text: 'cancel',
          role: 'cancel'
        },

      ]
    }).then(actionSh => {
      actionSh.present() ;
    })
  }

  private openModel(mode :'selected' | 'random') {
    this.modalCtrl.create(
      {component:CreatBookingComponent , componentProps: {selectedPlace: this.Place , theMode: mode}})
      .then(model =>{
      model.present();
       return model.onDidDismiss() ;
    }).then( data  => {
      console.log(data) ;
      this.bookingS.addBookin(
       data.data.placeid ,
       data.data.placetitile ,
       data.data.img ,
       data.data.guest ,
       data.data.firstname ,
       data.data.lastname ,
       data.data.from ,
       data.data.to ,
      );
    });
  }

  ngOnDestroy() {
    if( this.mySub ) {
      this.mySub.unsubscribe() ;
    }
  }

}

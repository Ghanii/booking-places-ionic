export class Place {
    constructor(
       public id : string ,
       public title: string,
       public description: string,
       public img: string,
       public cost: number,
       public dateFrom: Date ,
       public dateTo: Date,
       public userId: string

        ) {}
}
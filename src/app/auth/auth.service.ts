import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  private _isAuth = true ;
  private _userId = 'xyz' ;

  getAuth() {
    return this._isAuth ;
  }

  getUserId() {
    return this._userId ;
  }

  Login() {
    this._isAuth = true ;
  }
  Logout() {
    this._isAuth = false ;
  }
}

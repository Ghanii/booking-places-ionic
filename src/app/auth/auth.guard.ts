import { Injectable } from '@angular/core';
import {CanLoad, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Route , UrlSegment } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {
  constructor(
    private auth: AuthService,
    private router: Router
   ) {}
  canLoad(route: Route, segments: UrlSegment[]):
   boolean | Observable<boolean> | Promise<boolean> {
     if(!this.auth.getAuth()){
       this.router.navigateByUrl('/auth')
     }
    return this.auth.getAuth() ;
  }
}

import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import {Router} from '@angular/router' ;
import { LoadingController } from '@ionic/angular';

import { NgForm } from '@angular/forms' ;
@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {

  constructor(
    private auth: AuthService ,
    private router: Router,
    private loadingCtrl: LoadingController,
     ) { }

    isLoging = true ;
  ngOnInit() {
  }
  onLogin() {
    this.auth.Login();
    this.loadingCtrl.create({keyboardClose: true , message:'loading ...' , spinner:'bubbles'})
    .then(loading => {
      loading.present() ;
    });
    setTimeout( () => {
      this.loadingCtrl.dismiss() ;
      this.router.navigateByUrl('/places/tabs/discover');
    } , 2000);
  }

  SubmitAuth(form: NgForm) {
    console.log(form.form.value) ;
  }

  onSwitchMode() {
    this.isLoging = !this.isLoging ;
  }

}
